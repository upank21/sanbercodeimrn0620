// Jawaban Soal No. 1 (Range) 
function range(startNum, finishNum) {
  var array = [];

  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      array.push(i);
    }
    return array
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      array.push(i);
    }
    return array
  } else {
    return array = -1;
  }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("\n");

// Jawaban Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
  var array = [];

  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      array.push(i);
      i += (step - 1);
    }
    return array
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      array.push(i);
      i -= (step - 1);
    }
    return array
  } else {
    return array = -1;
  }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("\n");

// Jawaban No. 3 (Sum of Range)
function sum(startNum, finishNum = 1, step = 1) {
  var array = [];

  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      array.push(i);
      i += (step - 1);
    }

  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      array.push(i);
      i -= (step - 1);
    }

  } else {
    for (var i = startNum; i >= finishNum; i--) {
      array.push(i);
    }
  }

  var total = 0;
  for (j = 0; j < array.length; j++) {
    total += array[j];
  }
  return total
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("\n");

// Jawaban No.4(Array Multidimensi)
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling() {
  for (var i = 0; i < input.length; i++) {
    console.log("NOMOR ID : " + input[i][0]);
    console.log("Nama lengkap : " + input[i][1]);
    console.log("TTL : " + input[i][2] + " " + input[i][3]);
    console.log("Hobi : " + input[i][4]);
    console.log(" ");
  }
}
dataHandling();

// Jawaban No. 5 (Balik Kata)
function balikKata(kata) {
  var huruf = kata;
  var kataBaru = '';

  for (var i = huruf.length - 1; i >= 0; i--) {
    kataBaru = kataBaru + huruf[i];
  }
  return kataBaru;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log("\n");

// Jawaban No. 6 (Metode Array)
var input = ["0001", "Roman Alamsyah ", " Bandar Lampung", "21/05/1989", "Membaca"];

//Inisialisasi
var namaAwal = input[1];

// Input tambahan data
var namaBelakang = "Elsharawy";
var jenisKelamin = "Pria";
var provinsi = "Provinsi";
var sekolah = "SMA Internasional Metro";

function dataHandling2(namaBelakang, jenisKelamin, provinsi, sekolah) {
  var nama = input[1] + namaBelakang;
  var provinsi = provinsi + input[2];

  input.splice(1, 2, nama, provinsi);
  input.splice(4, 1, jenisKelamin, sekolah);
  console.log(input);

  var splitBln = input[3].split("/");
  var bln = parseInt(splitBln[1]);

  switch (bln) {
    case 01:
      console.log("Januari");
      break;
    case 02:
      console.log("Februari");
      break;
    case 03:
      console.log("Maret");
      break;
    case 04:
      console.log("April");
      break;
    case 05:
      console.log("Mei");
      break;
    case 06:
      console.log("Juni");
      break;
    case 07:
      console.log("Juli");
      break;
    case 08:
      console.log("Agustus");
      break;
    case 09:
      console.log("September");
      break;
    case 10:
      console.log("Oktober");
      break;
    case 11:
      console.log("November");
      break;
    case 12:
      console.log("Desember");
      break;
    default:
      break;
  }
  var bulan = splitBln.sort(function (b, a) {
    return a - b
  });
  console.log(bulan);

  var tgl = input[3].split("/");
  var tglJoin = tgl.join("-");
  console.log(tglJoin);
  console.log(namaAwal);

}

dataHandling2(namaBelakang, jenisKelamin, provinsi, sekolah);