// Jawaban soal 1 Looping While
var angka = 2;
var jumlahPerulangan = 20;

console.log("Looping Pertama");
while (angka <= jumlahPerulangan) {
  console.log(angka + " - I Love coding")
  angka += 2;
}
console.log("Looping Kedua");
angka -= 2;
while (angka > 0) {
  console.log(angka + " - I will become a mobile developer")
  angka = angka - 2;
}
console.log("\n");


// Jawaban soal 2 Looping menggunakan for
var angkaAwal = 1;
var jumlahPerulangan = 20;

for (angkaAwal; angkaAwal <= jumlahPerulangan; angkaAwal++) {
  if ((angkaAwal % 3) === 0 && (angkaAwal % 2) === 1) { //cek angka ganjil dan kelipatan 3
    console.log(angkaAwal + " - I Love Coding");
  } else if ((angkaAwal % 2) === 0) { //cek angka genap
    console.log(angkaAwal + " - Berkualitas");
  } else { //cek angka ganjil
    console.log(angkaAwal + " - Santai");
  }
}
console.log("\n");

// Jawaban Soal 3 Membuat Persegi Panjang #
var panjang = 8; //isi nilai panjang
var lebar = 4; //isi nilai lebar
var string = "";

for (var i = 1; i <= lebar; i++) {
  for (var j = 1; j <= panjang; j++) {
    string += "#";
  }
  string += "\n";
}
console.log(string);

// Jawaban Soal 4 Membuat Tangga #
var tangga = 7; //isi jumlah tangga
var string = "";

for (var i = 1; i <= tangga; i++) {
  for (var j = 1; j <= i; j++) {
    string += "#";
  }
  string += "\n";
}
console.log(string);

// Jawaban Soal 5 Membuat Papan Catur #
var panjang = 8; //isi nilai panjang
var lebar = 8; //isi nilai lebar
var string = "";

for (var i = 1; i <= lebar; i++) {
  for (var j = 1; j <= panjang; j++) {
    if ((j % 2 === 0 && i % 2 === 0) || (j % 2 === 1 && i % 2 === 1)) {
      string += "#";
    } else {
      string += " ";
    }
  }
  string += "\n";
}
console.log(string);