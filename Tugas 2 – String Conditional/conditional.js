//Jawaban soal game werewolf!

// Output untuk Input nama = '' dan peran = ''
var nama = "Erfan"; // Silahkan isi nama
var peran = "Guard"; // Silahkan isi peran karakter game

// Untuk menghilangkan spasi di awal dan di akhir kalimat
var newNama = nama.trim();
var newPeran = peran.trim();

if (newNama == "") {
  console.log("Nama harus diisi!");
} else if (newPeran == "") {
  console.log("Halo " + newNama + ", Pilih peranmu untuk memulai game!");
} else if (newNama != "" && newPeran != "") {
  if (newPeran === "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + newNama);
    console.log("Halo Penyihir " + newNama + ",kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (newPeran === "Guard") {
    console.log("Selamat datang di Dunia Werewolf, " + newNama);
    console.log("Halo Guard " + newNama + ",kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (newPeran === "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + newNama);
    console.log("Halo Werewolf " + newNama + ",Kamu akan memakan mangsa setiap malam!");
  } else {
    console.log("Hai " + newNama + " Peran yang kamu pilih tidak ada dalam karakter game");
  }
}



//Jawaban soal Format tanggal
var tanggal = 12; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1988; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

// scope variabel yang diizinkan diinput
if (tanggal < 1 || tanggal > 31) {
  console.log("Tanggal yang kamu input harus diantara angka 1 s.d 31");
}
if (bulan < 1 || bulan > 12) {
  console.log("Bulan yang kamu input harus diantara angka 1 s.d 12");
}
if (tahun < 1900 || tahun > 2200) {
  console.log("Tahun yang kamu input harus diantara tahun 1900 s.d 2200");
}

switch (bulan) {
  case 1:
    console.log(tanggal + " " + "Januari" + " " + tahun)
    break;
  case 2:
    console.log(tanggal + " " + "Februari" + " " + tahun)
    break;
  case 3:
    console.log(tanggal + " " + "Maret" + " " + tahun)
    break;
  case 4:
    console.log(tanggal + " " + "April" + " " + tahun)
    break;
  case 5:
    console.log(tanggal + " " + "Mei" + " " + tahun)
    break;
  case 6:
    console.log(tanggal + " " + "Juni" + " " + tahun)
    break;
  case 7:
    console.log(tanggal + " " + "Juli" + " " + tahun)
    break;
  case 8:
    console.log(tanggal + " " + "Agustus" + " " + tahun)
    break;
  case 9:
    console.log(tanggal + " " + "September" + " " + tahun)
    break;
  case 10:
    console.log(tanggal + " " + "Oktober" + " " + tahun)
    break;
  case 11:
    console.log(tanggal + " " + "November" + " " + tahun)
    break;
  case 12:
    console.log(tanggal + " " + "Desember" + " " + tahun)
    break;

  default:
    break;
}